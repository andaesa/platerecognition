this script is based on the answer provided in http://stackoverflow.com/questions/36407608/recognize-the-characters-of-license-plate

The problem with script 'Training1.py' is that it did not properly extract each digits or letters from given image.
I skipped to run script 'Training1.py' and manually cropped and resized each digits and letters from given image.

The script 'generatedata.py' works fine.

I managed to get script 'Platedetect.py' to run properly. It worked fine with the sample image given from above link but still have minor error such as 
the script failed to recognize between '8' and 'B'.
I tested for other sample image of car plate number but apparently the script still failed to properly recognized the plate even though there was no error
when running it.

The other sample image besides one from the link I got it from Google Image.